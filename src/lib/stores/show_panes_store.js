import { writable } from 'svelte/store';

export const show_bookmarks = writable(false);
export const show_layout_forms = writable(false);
export const show_server_status = writable(false);
export const show_info = writable(false);

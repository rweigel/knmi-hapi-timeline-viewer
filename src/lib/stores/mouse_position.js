import { readable, writable, get } from 'svelte/store';

let svg_element;
export const set_svg_element = function(el) {
    svg_element = el;
}

// Get point in global SVG space
function toSVGPoint(svg, x, y){
  let pt = new DOMPoint(x, y);
  return pt.matrixTransform(svg.getScreenCTM().inverse());
}

export const mouse_position = readable({x:0, y:0}, (set) => {
    document.body.addEventListener("mousemove", move);

    function move(event) {
        var loc = toSVGPoint(svg_element, event.clientX, event.clientY);
        set({
            x: loc.x,
            y: loc.y,
        });
    }

    return () => {
        document.body.removeEventListener("mousemove", move);
    }
})

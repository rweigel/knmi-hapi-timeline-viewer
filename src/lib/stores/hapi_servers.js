import { writable } from 'svelte/store';

const initial_hapi_servers = [
    {value: "https://data.spaceweather.knmi.nl/hapi", name: "KNMI HAPI server"},
    {value: "https://cdaweb.gsfc.nasa.gov/hapi", name: "NASA CDAWeb"},
    {value: "https://iswa.gsfc.nasa.gov/IswaSystemWebApp/hapi", name: "NASA CCMC ISWA"},
    {value: "https://hapi-server.org/servers/SSCWeb/hapi", name: "NASA SSCWeb (Satellite Situation Centre)"},
    {value: "https://planet.physics.uiowa.edu/das/das2Server/hapi", name: "Univ. Iowa DAS2 "},
    {value: "https://lasp.colorado.edu/lisird/hapi", name: "Univ. Colorado LASP Interactive Solar Irradiance Datacenter"},
    {value: "http://amda.irap.omp.eu/service/hapi", name: "IRAP AMDA (Automated Multi-Dataset Analysis)"},
    {value: "https://vires.services/hapi", name: "ESA ViRES for Swarm"},
    {value: "https://imag-data.bgs.ac.uk/GIN_V1/hapi", name: "Intermagnet"},
    {value: "https://csatools.esac.esa.int/HapiServer/hapi", name: "Cluster Science Archive"},
];

export const hapi_servers = writable(initial_hapi_servers);

import { writable } from 'svelte/store';

let initial_layout = '/viewer/layout_real_time_space_weather.json';
if (window.location.hash !== "") {
    let urlParams = new URLSearchParams(window.location.hash.substring(1));
    if (urlParams.has('layout')) {
        initial_layout = urlParams.get('layout');
    }
} else {
    let urlParams = new URLSearchParams(window.location.search);
    if (urlParams.has('layout')) {
        initial_layout = '/viewer/layout_' + urlParams.get('layout') + '.json';
    }
}

export const layout_file = writable(initial_layout)
export const layout = writable();

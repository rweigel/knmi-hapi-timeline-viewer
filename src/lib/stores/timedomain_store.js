import { writable } from 'svelte/store';
import dayjs from 'dayjs';
import duration from 'dayjs/plugin/duration';
dayjs.extend(duration);

export const timeDomain = writable([undefined, undefined]);
timeDomain.set([dayjs().startOf('year'), dayjs().endOf('year')]);

const parse_time_or_duration_string = function(string) {
    let duration = dayjs.duration(string);
    let dayjsobj = dayjs(string);
    if (dayjsobj.isValid()) {

    } else if (dayjs.isDuration(duration)) {
        if (string.startsWith('-')) {
            dayjsobj = dayjs().subtract(duration);
        } else {
            dayjsobj = dayjs().add(duration);
        }
    } else {
        console.log(`${string} could not be parsed`);
        dayjsobj = dayjs();
    }
    return dayjsobj;
}

export const time_domain_from_zoom_obj = function(zoom_obj) {
    timeDomain.set([parse_time_or_duration_string(zoom_obj.tmin),
                    parse_time_or_duration_string(zoom_obj.tmax)]);
}

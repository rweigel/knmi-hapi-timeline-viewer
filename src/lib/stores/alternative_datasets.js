import dayjs from 'dayjs';
import duration from 'dayjs/plugin/duration';
dayjs.extend(duration);

export const id_split_cadence_knmi_convention = function(dataset_id) {
    // HAPI dataset_ids are strings of the form: solar_wind_plasma_PT3H
    // This function takes a dataset_id, and checks whether the last part
    // (if split by underscores) is an ISO duration string. If so, parse the
    // ISO duration and return a cleaned-up dataset_id, together with the
    // duration string and parsed duration.
    const split_id = dataset_id.split('_');
    const last_part = split_id[split_id.length-1];
    // Check if last part is a cadence
    if (last_part.endsWith('Hz')) {
        return {'dataset_id': dataset_id,
                'dataset_id_root': split_id.slice(0, split_id.length-1).join("_"),
                'cadence_info': {'cadence_string': last_part,
                                 'cadence_sec': 1/(last_part.substring(0,last_part.length-2))}};

    }
    const cadence_sec = dayjs.duration(last_part).asSeconds()
    if (isNaN(cadence_sec)) {
        return {'dataset_id': dataset_id,
                'dataset_id_root': undefined,
                'cadence_info': {'cadence_string': undefined,
                                 'cadence_sec': undefined}};
    } else {
        return {'dataset_id': dataset_id,
                'dataset_id_root': split_id.slice(0, split_id.length-1).join("_"),
                'cadence_info': {'cadence_string': last_part,
                                 'cadence_sec': cadence_sec}};
    }
}


export const find_alternatives = function(plot_element, catalog, info) {
    let alternative_datasets = [];
    if (plot_element.data.cadence_alternative_datasets_mode == "knmi") {
        let dataset_cadence_info = id_split_cadence_knmi_convention(plot_element.data.dataset)
        if (dataset_cadence_info.dataset_id_root == undefined) {
            return [dataset_cadence_info];
        }
        catalog.catalog.forEach((item) => {
            let alt_dataset_cadence_info = id_split_cadence_knmi_convention(item.id);
            if (alt_dataset_cadence_info.dataset_id_root == dataset_cadence_info.dataset_id_root) {
                alternative_datasets.push(alt_dataset_cadence_info);
            }
        });
        if (alternative_datasets.length == 0) {
            return [dataset_cadence_info];
        }
    } else if (plot_element.data.cadence_alternative_datasets_mode == "intermagnet") {
        let split_name = plot_element.data.dataset.split('/');
        if (split_name[2] == 'PT1M') {
            // For 1 minute Intermagnet data, check if there is a 1-second version in the catalog
            alternative_datasets.push(
                {'dataset_id': plot_element.data.dataset,
                 'cadence_info': {'cadence_string': 'PT1M',
                                  'cadence_sec': 60}}
            );
            split_name[2] = 'PT1S';
            let alternative_id = split_name.join('/');
            if (catalog.catalog.filter(ds=>ds.id == alternative_id).length > 0) {
                alternative_datasets.push(
                    {'dataset_id': alternative_id,
                     'cadence_info': {'cadence_string': 'PT1S',
                                      'cadence_sec': 1}}
                );
            }
        }
    } else if (plot_element.data.cadence_alternative_datasets_mode == "x_relations") {
        alternative_datasets.push({'dataset_id': plot_element.data.dataset,
                                   'cadence_info': {'cadence_string': info['cadence'],
                                                    'cadence_sec': dayjs.duration(info['cadence']).asSeconds()}});
        if ('x_relations' in info) {
            info['x_relations'].forEach((item) => {
                if ('cadence' in item) {
                    alternative_datasets.push(
                        {'dataset_id': item.id,
                         'cadence_info': {'cadence_string': item['cadence'],
                                          'cadence_sec': dayjs.duration(item['cadence']).asSeconds()}
                        }
                    );
                }
            });
        }
    } else {
        return [{'dataset_id': plot_element.data.dataset}];
    }
    return alternative_datasets.sort((a, b) => (a['cadence_info']['cadence_sec'] > b['cadence_info']['cadence_sec']) ? 1 : -1);
}

export const select_dataset_from_cadence_info = function(alternative_datasets, time_domain, max_points) {
    let selected_dataset;
    let selected_dataset_points = 0;
    if (alternative_datasets.length > 1) {
        for (let i=0; i < alternative_datasets.length; i++) {
            let points = (time_domain[1] - time_domain[0])/(alternative_datasets[i].cadence_info.cadence_sec*1000);
            if (points > selected_dataset_points && points <= max_points) {
                selected_dataset = alternative_datasets[i];
                selected_dataset_points = points;
            }
        }
        if (typeof(selected_dataset)=='undefined') {
            selected_dataset = alternative_datasets[alternative_datasets.length-1];
        }
    } else {
        selected_dataset = alternative_datasets[0];
    }
    return selected_dataset;
}

import * as d3 from 'd3';

// Compute the display-height and y-coordinates of each plotting area from their given height and margins

export const computeSubplotLayouts = function(layoutData, windowInnerHeight) {
    // The total height of the plot area and margins are needed to compute the display height,
    // if the fillVertical option is used to make them fill up the available vertical space
    let subplots = layoutData.subplots;
    let fillVertical = layoutData.options.fill_vertical;
    let tickSpacing = 25;
    let totalPlotAreaHeight = 0;
    let totalMarginHeight = 0;

    // First compute the total specified height
    subplots.forEach(function(subplot) {
        if (subplot.visible) {
            totalPlotAreaHeight = totalPlotAreaHeight + subplot.height;
            totalMarginHeight = totalMarginHeight + subplot.margins.top + subplot.margins.bottom;
        }
    });
    let totalHeights = {plotarea: totalPlotAreaHeight,
                        margins: totalMarginHeight + layoutData.global_margins.top + layoutData.global_margins.bottom};

    // Now compute the subplot layout
    let ynew = layoutData.global_margins.top;
    let y;
    let h;
    let subplotLayouts = [];
    subplots.forEach(function(subplot) {
        if (subplot.visible) {
            y = ynew + subplot.margins.top;
            if (fillVertical) {
                h = subplot.height * (windowInnerHeight-totalHeights.margins)/totalHeights.plotarea;
            } else {
                h = subplot.height;
            }

            // Y-axis
            let yScale;
            let ticks;
            let logTicks;

            let domain;
            let range = [y+h, y];
            if (subplot.scale === "log10") {
                domain = [Math.pow(10, subplot.ydomain.default[0]), Math.pow(10, subplot.ydomain.default[1])];
                yScale = d3.scaleLog().domain(domain).range(range);
                ticks = yScale.ticks(h/tickSpacing);
                logTicks = ticks.map(logComponents);
            } else {
                domain = [subplot.ydomain.default[0], subplot.ydomain.default[1]];
                yScale = d3.scaleLinear().domain(domain).range(range);
                ticks = yScale.ticks(h/tickSpacing);
            }
            subplot['computed']['y'] = y;
            subplot['computed']['h'] = h,
            subplot['computed']['yScale'] = yScale;
            subplot['computed']['ticks'] = ticks;
            subplot['computed']['logTicks'] = logTicks;

            ynew = ynew + subplot.margins.top + h + subplot.margins.bottom;
        }
    });
    return subplots;
    // return true; No need to return anything, as all modifications are in the layoutData object
}

function logComponents(value) {
    let logValue = Math.log10(value);
    let exponent = Math.floor(logValue);
    let multiplier = Math.pow(10, -1*exponent+logValue).toFixed(0);
    return {'value': value, 'exponent': exponent, 'multiplier': multiplier};
}

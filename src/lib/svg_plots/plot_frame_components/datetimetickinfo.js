import * as d3 from 'd3';

export const getDateTimeTickInfo = function(xScale) {
    let pixelWidth = xScale.range()[1]-xScale.range()[0];
    let standardPixelWidth = 1280;
    let daysInView = (xScale.domain()[1]-xScale.domain()[0])/1e3/3600/24;
    daysInView *= standardPixelWidth / pixelWidth; // scale for on-screen pixel width
    let interval_p, interval_s, tickformat_p, tickformat_s;
    let visibility_annot_p = "visible";
    if (daysInView < 0.00035) {
      interval_p = d3.utcSecond;
      interval_s = d3.utcDay;
      tickformat_p = "%H:%M:%S";
      tickformat_s = "%Y-%m-%d";
    } else if (daysInView < 0.001) {
      interval_p = d3.utcSecond.every(5);
      interval_s = d3.utcDay;
      tickformat_p = "%H:%M:%S";
      tickformat_s = "%Y-%m-%d";
    } else if (daysInView < 0.0025) {
      interval_p = d3.utcSecond.every(10);
      interval_s = d3.utcDay;
      tickformat_p = "%H:%M:%S";
      tickformat_s = "%Y-%m-%d";
    } else if (daysInView < 0.025) {
      interval_p = d3.utcMinute;
      interval_s = d3.utcDay;
      tickformat_p = "%H:%M";
      tickformat_s = "%Y-%m-%d";
    } else if (daysInView < 0.1) {
        interval_p = d3.utcMinute.every(5);
        interval_s = d3.utcDay;
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (daysInView < 0.3) {
        interval_p = d3.utcMinute.every(15);
        interval_s = d3.utcDay;
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (daysInView < 0.5) {
        interval_p = d3.utcMinute.every(30);
        interval_s = d3.utcDay;
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (daysInView < 1) {
        interval_p = d3.utcHour.every(1);
        interval_s = d3.utcDay;
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (daysInView < 3) {
        interval_p = d3.utcHour.every(3);
        interval_s = d3.utcDay;
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (daysInView < 6) {
        interval_p = d3.utcHour.every(6);
        interval_s = d3.utcDay;
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (daysInView < 12) {
        interval_p = d3.utcHour.every(12);
        interval_s = d3.utcDay;
        tickformat_p = "%H:%M";
        tickformat_s = "%Y-%m-%d";
    } else if (daysInView < 0.15*365) {
        interval_p = d3.utcDay;
        interval_s = d3.utcMonth;
        tickformat_p = "%d";
        tickformat_s = "%b %Y";
    } else if (daysInView < 0.3*365) {
        interval_p = d3.utcDay;
        interval_s = d3.utcMonth;
        tickformat_p = "%d";
        tickformat_s = "%b %Y";
        visibility_annot_p = "hidden";
    } else if (daysInView < 3*365) {
        interval_p = d3.utcMonth;
        interval_s = d3.utcYear;
        tickformat_p = "%b";
        tickformat_s = "%Y";
    } else if (daysInView < 30*365) {
        interval_p = d3.utcYear;
        interval_s = d3.utcYear.every(10);
        tickformat_p = "%Y";
        tickformat_s = "%Ys";
    } else {
      interval_p = d3.utcYear.every(10);
      interval_s = d3.utcYear.every(100);
      tickformat_p = "%Ys";
      tickformat_s = "%Ys";
    }
    let tickInfo = {
        'interval_p': interval_p,
        'interval_s': interval_s,
        'tickformat_p': tickformat_p,
        'tickformat_s': tickformat_s,
        'visibility_annot_p': visibility_annot_p
    }
    return tickInfo;
}

import { layout } from '../stores/layout_store';

let $layout;

const set_subplot_height = function(args) {
    layout.update(l => {
        l.subplots[args.i_subplot].height = 50 + 950/127 * args.value;
        return l
    });
}

const set_subplot_ybounds = function(args) {
    layout.update(l => {
        let new_ybound;
        let subplot = l.subplots[args.i_subplot];
        if (subplot.scale == 'log10') {
            new_ybound = 1*subplot.ydomain.bounds[0] + args.value/127 * (1*subplot.ydomain.bounds[1] - 1*subplot.ydomain.bounds[0]);
        } else {
            new_ybound = 1*subplot.ydomain.bounds[0] + args.value/127 * (1*subplot.ydomain.bounds[1] - 1*subplot.ydomain.bounds[0]);
        }
        if (subplot.ydomain)

        if (subplot.ydomain.mirror) {
            subplot.ydomain.default[1]=1*new_ybound;
            subplot.ydomain.default[0]=-1*new_ybound;
        } else {
            subplot.ydomain.default[args.minmax]=1*new_ybound;
        }
        return l
    });
}

const toggle_subplot_x_annotation = function(args) {
    if (args.value == 127) {
        layout.update(l => {
            if (l.subplots[args.i_subplot].xaxis_annotation == 'visible') {
                l.subplots[args.i_subplot].xaxis_annotation = 'hidden'
            } else {
                l.subplots[args.i_subplot].xaxis_annotation = 'visible'
            }
            return l
        });
    }
}

const toggle_subplot_legend = function(args) {
    if (args.value == 127) {
        layout.update(l => {
            if (l.subplots[args.i_subplot].legend_visible == true) {
                l.subplots[args.i_subplot].legend_visible = false;
            } else {
                l.subplots[args.i_subplot].legend_visible = true;
            }
            return l
        });
    }
}


const set_subplot_margin_bottom = function(args) {
    layout.update(l => {
        l.subplots[args.i_subplot].margins.bottom = args.value;
        return l
    });
}



const midi_map = {
    '176_14': { // Knob 1
        f: set_subplot_height,
        i_subplot: 0,
    },
    '176_15': { // Knob 2
        f: set_subplot_height,
        i_subplot: 1,
    },
    '176_16': { // Knob 3
        f: set_subplot_height,
        i_subplot: 2,
    },
    '176_17': { // Knob 4
        f: set_subplot_height,
        i_subplot: 3,
    },
    '176_18': { // Knob 5
        f: set_subplot_height,
        i_subplot: 4,
    },
    '176_19': { // Knob 6
        f: set_subplot_height,
        i_subplot: 5,
    },
    '176_20': { // Knob 7
        f: set_subplot_height,
        i_subplot: 6,
    },
    '176_21': { // Knob 8
        f: set_subplot_height,
        i_subplot: 7,
    },
    '176_22': { // Knob 9
        f: set_subplot_height,
        i_subplot: 8,
    },
    '176_2': { // Slider 1
        f: set_subplot_ybounds,
        i_subplot: 0,
        minmax: 1,
    },
    '176_3': { // Slider 2
        f: set_subplot_ybounds,
        i_subplot: 1,
        minmax: 1,
    },
    '176_4': { // Slider 3
        f: set_subplot_ybounds,
        i_subplot: 2,
        minmax: 1,
    },
    '176_5': { // Slider 4
        f: set_subplot_ybounds,
        i_subplot: 3,
        minmax: 1,
    },
    '176_6': { // Slider 5
        f: set_subplot_ybounds,
        i_subplot: 4,
        minmax: 1,
    },
    '176_8': { // Slider 6
        f: set_subplot_ybounds,
        i_subplot: 5,
        minmax: 1,
    },
    '176_9': { // Slider 7
        f: set_subplot_ybounds,
        i_subplot: 6,
        minmax: 1,
    },
    '176_12': { // Slider 8
        f: set_subplot_ybounds,
        i_subplot: 7,
        minmax: 1,
    },
    '176_13': { // Slider 9
        f: set_subplot_ybounds,
        i_subplot: 8,
        minmax: 1,
    },
    '176_23': { // Top button 1
        f: toggle_subplot_x_annotation,
        i_subplot: 0,
    },
    '176_24': { // Top button 2
        f: toggle_subplot_x_annotation,
        i_subplot: 1,
    },
    '176_25': { // Top button 3
        f: toggle_subplot_x_annotation,
        i_subplot: 2,
    },
    '176_26': { // Top button 4
        f: toggle_subplot_x_annotation,
        i_subplot: 3,
    },
    '176_27': { // Top button 5
        f: toggle_subplot_x_annotation,
        i_subplot: 4,
    },
    '176_28': { // Top button 6
        f: toggle_subplot_x_annotation,
        i_subplot: 5,
    },
    '176_29': { // Top button 7
        f: toggle_subplot_x_annotation,
        i_subplot: 6,
    },
    '176_30': { // Top button 8
        f: toggle_subplot_x_annotation,
        i_subplot: 7,
    },
    '176_31': { // Top button 9
        f: toggle_subplot_x_annotation,
        i_subplot: 8,
    },
    '176_33': { // Bottom button 1
        f: toggle_subplot_legend,
        i_subplot: 0,
    },
    '176_34': { // Bottom button 2
        f: toggle_subplot_legend,
        i_subplot: 1,
    },
    '176_35': { // Bottom button 3
        f: toggle_subplot_legend,
        i_subplot: 2,
    },
    '176_36': { // Bottom button 4
        f: toggle_subplot_legend,
        i_subplot: 3,
    },
    '176_37': { // Bottom button 5
        f: toggle_subplot_legend,
        i_subplot: 4,
    },
    '176_38': { // Bottom button 6
        f: toggle_subplot_legend,
        i_subplot: 5,
    },
    '176_39': { // Bottom button 7
        f: toggle_subplot_legend,
        i_subplot: 6,
    },
    '176_40': { // Bottom button 8
        f: toggle_subplot_legend,
        i_subplot: 7,
    },
    '176_41': { // Bottom button 9
        f: toggle_subplot_legend,
        i_subplot: 8,
    }
}


export const setup_midi_input = function() {
    let midi = null; // global MIDIAccess object
    let nanoKontrol_
    function listInputsAndOutputs(midiAccess) {
      for (const entry of midiAccess.inputs) {
        const input = entry[1];
        console.log(
          `Input port [type:'${input.type}']` +
            ` id:'${input.id}'` +
            ` manufacturer:'${input.manufacturer}'` +
            ` name:'${input.name}'` +
            ` version:'${input.version}'`
        );
      }

      for (const entry of midiAccess.outputs) {
        const output = entry[1];
        console.log(
          `Output port [type:'${output.type}'] id:'${output.id}' manufacturer:'${output.manufacturer}' name:'${output.name}' version:'${output.version}'`
        );
      }
    }

    function onMIDIMessage(event) {
      let str = `MIDI message received [${event.data.length} bytes]: `;
      for (const character of event.data) {
        str += `${character}  `;
      }
      // console.log(event.data);
      if (event.data.length==3) {
          let midi_data_id = `${event.data[0]}_${event.data[1]}`;
          if (midi_data_id in midi_map) {
            let action = midi_map[midi_data_id];
            let args = {...action, value: event.data[2]}
            // console.log(action, args);
            action.f(args)
            return;
          }
      };

    }

    function startLoggingMIDIInput(midiAccess) {
      midiAccess.inputs.forEach((entry) => {
        entry.onmidimessage = onMIDIMessage;
      });
    }

    function onMIDISuccess(midiAccess) {
      console.log("MIDI ready!");
      midi = midiAccess; // store in the global (in real usage, would probably keep in an object instance)
      // listInputsAndOutputs(midi);
      startLoggingMIDIInput(midi);

    }

    function onMIDIFailure(msg) {
      console.error(`Failed to get MIDI access - ${msg}`);
    }

    if ('requestMIDIAccess' in navigator) {
        navigator.requestMIDIAccess().then(onMIDISuccess, onMIDIFailure);
    } else {
        console.log("Browser does not seem to support MIDI");
    }

}

export const subplot_template_empty = {
	  "title": "New subplot",
	  "visible": true,
	  "minimized": true,
	  "ydomain": {
		  "default": [0, 350],
		  "bounds": [-500, 500]
	  },
	  "y_label": "New label",
	  "scale": "linear",
      "ytick_format": "d",
	  "xaxis_annotation": "visible",
	  "yaxis_annotation": "visible",
	  "height": 150,
	  "annot_options": {
		  "annot_horizontal_grid_lines": [
		  ],
		  "annot_text_items": [
		  ]
	  },
	  "margins": {
		  "top": 30,
		  "bottom": 40
	  },
	  "plot_elements": [
	  ],
  }

export const subplot_template_xray_flux = {
  "title": "GOES X-ray flux",
  "visible": true,
  "legend_visible": true,
  "ydomain": {
    "default": [
      -9,
      -2
    ],
    "bounds": [
      "-9",
      "-2"
    ]
  },
  "y_label": "X-ray flux (W/m2)",
  "scale": "log10",
  "ytick_format": "d",
  "xaxis_annotation": "hidden",
  "yaxis_annotation": "visible",
  "height": 150,
  "annot_options": {
    "annot_horizontal_grid_lines": [
      {
        "y": 1e-06
      },
      {
        "y": 1e-05
      },
      {
        "y": 0.0001
      },
      {
        "y": 0.001
      }
    ],
    "annot_text_items": [
      {
        "y": 3.163e-06,
        "align": "left",
        "text": "C"
      },
      {
        "y": 3.163e-05,
        "align": "left",
        "text": "M"
      },
      {
        "y": 0.0003163,
        "align": "left",
        "text": "X"
      }
    ]
  },
  "margins": {
    "top": 30,
    "bottom": 10
  },
  "plot_elements": [
    {
      "title": "X-ray flux long",
      "visible": true,
      "plot_options": {
        "plot_type": "area",
        "stroke": "#8f9cbc",
        "stroke-width": "2",
        "stroke-opacity": 1,
        "fill": "#b5c7f2",
        "fill-opacity": 0.52,
        "curve_type": "linear",
        "baseline_mode": "constant",
        "baseline": "1e-12",
        "baseline_stroke": "#000000",
        "baseline_stroke-width": "0",
        "baseline_stroke-opacity": "0"
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "xray_flux_rt",
        "parameter": "xray_flux_long",
        "time_parameter": "time",
        "cadence_alternative_datasets_mode": "x_relations",
        "parameter_multiplier": 1,
        "refetch_interval_seconds": 60,
        "zoom_detail_factor": 2
      },
      "data_transforms": {
        "parameter_multiplier": 0,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "X-ray flux short",
      "visible": true,
      "plot_options": {
        "plot_type": "area",
        "stroke": "#7c8498",
        "stroke-width": 0,
        "stroke-opacity": 1,
        "fill": "#b8c5e5",
        "fill-opacity": 1,
        "curve_type": "linear",
        "baseline_mode": "constant",
        "baseline": "1e-12",
        "baseline_stroke": "#000000",
        "baseline_stroke-width": "0",
        "baseline_stroke-opacity": "0"
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "xray_flux_rt",
        "parameter": "xray_flux_short",
        "time_parameter": "time",
        "cadence_alternative_datasets_mode": "x_relations",
        "parameter_multiplier": 1,
        "refetch_interval_seconds": 60,
        "zoom_detail_factor": 2
      },
      "data_transforms": {
        "parameter_multiplier": 1,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    }
  ]
}

export const subplot_template_hp30_index =     {
  "title": "GFZ-Potsdam Hp30 index and NOAA Kp forecast",
  "visible": true,
  "legend_visible": false,
  "ydomain": {
    "default": [
      0,
      "9"
    ],
    "bounds": [
      "0",
      "15"
    ]
  },
  "y_label": "",
  "scale": "linear",
  "ytick_format": "d",
  "xaxis_annotation": "visible",
  "yaxis_annotation": "visible",
  "height": 80,
  "annot_options": {
    "annot_horizontal_grid_lines": [],
    "annot_text_items": []
  },
  "margins": {
    "top": 10,
    "bottom": 30
  },
  "plot_elements": [
    {
      "title": "Hp30 index",
      "visible": true,
      "plot_options": {
        "plot_type": "bars",
        "stroke": "#000000",
        "stroke-width": "0",
        "stroke-opacity": "1",
        "fill": "#888888",
        "fill-opacity": "1",
        "curve_type": "linear",
        "bar_offset_seconds": 1800,
        "bar_padding": 0.2,
        "bar_text": true,
        "fill_colors": [
          {
            "threshold": 4.6,
            "color": "#88CC66"
          },
          {
            "threshold": 5.6,
            "color": "#FFF033"
          },
          {
            "threshold": 6.6,
            "color": "#FFBB44"
          },
          {
            "threshold": 7.6,
            "color": "#DD9922"
          },
          {
            "threshold": 8.6,
            "color": "#FF2020"
          },
          {
            "threshold": 9.6,
            "color": "#A02020"
          },
          {
            "threshold": 1000,
            "color": "#300000"
          }
        ]
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "hp30_index",
        "parameter": "Hp30",
        "time_parameter": "time",
        "cadence_alternative_datasets_mode": "x_relations",
        "refetch_interval_seconds": 600,
        "zoom_detail_factor": 2
      },
      "data_transforms": {}
    }
  ]
}

export const subplot_template_imf =     {
  "title": "Interplanetary Magnetic Field near Sun-Earth L1",
  "visible": true,
  "legend_visible": true,
  "ydomain": {
    "default": [
      "-30",
      "30"
    ],
    "bounds": [
      "-80",
      "80"
    ],
    "step": 1,
    "mirror": true
  },
  "scale": "linear",
  "ytick_format": "d",
  "y_label": "IMF (nT)",
  "xaxis_annotation": "hidden",
  "yaxis_annotation": "visible",
  "height": 280,
  "margins": {
    "top": 10,
    "bottom": 10
  },
  "plot_elements": [
    {
      "title": "IMF Bt",
      "visible": true,
      "plot_options": {
        "plot_type": "area",
        "baseline_mode": "mirror_zero_x",
        "curve_type": "linear",
        "stroke": "#000000",
        "stroke-width": "0",
        "stroke-opacity": "1",
        "fill": "#d6d6d6",
        "fill-opacity": 1
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "solar_wind_mag_rt",
        "parameter": "bt",
        "time_parameter": "time",
        "cadence_alternative_datasets_mode": "x_relations",
        "refetch_interval_seconds": 60,
        "zoom_detail_factor": 2
      },
      "data_transforms": {
        "parameter_multiplier": 1,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "IMF GSM Bz",
      "visible": true,
      "plot_options": {
        "plot_type": "area",
        "baseline_mode": "constant",
        "baseline": 0,
        "baseline_stroke": "#000000",
        "baseline_stroke-width": "0",
        "baseline_stroke-opacity": "0",
        "parameter_multplier": 1,
        "curve_type": "linear",
        "stroke": "#000000",
        "stroke-width": "0",
        "stroke-opacity": "1",
        "fill": "#C4472E",
        "fill-opacity": "1"
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "solar_wind_mag_rt",
        "parameter": "bz_gsm",
        "time_parameter": "time",
        "cadence_alternative_datasets_mode": "x_relations",
        "refetch_interval_seconds": 60,
        "zoom_detail_factor": 2
      },
      "data_transforms": {
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "IMF GSM By",
      "visible": true,
      "plot_options": {
        "plot_type": "line",
        "stroke": "#008ce6",
        "stroke-width": "1",
        "stroke-opacity": "1",
        "fill": "#AAAAAA",
        "fill-opacity": "1",
        "curve_type": "linear"
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "solar_wind_mag_rt",
        "parameter": "by_gsm",
        "time_parameter": "time",
        "cadence_alternative_datasets_mode": "x_relations",
        "refetch_interval_seconds": 60,
        "zoom_detail_factor": 2
      },
      "data_transforms": {
        "parameter_multiplier": 1,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    }
  ]
}

export const subplot_template_sunspots =     {
  "title": "Sunspot number, observed: WDC-SILSO, Royal Observatory of Belgium / forecast: NASA MSFC",
  "visible": true,
  "ydomain": {
    "default": [
      0,
      300
    ],
    "bounds": [
      0,
      500
    ],
    "mirror": false
  },
  "y_label": "Sunspot number",
  "scale": "linear",
  "ytick_format": "d",
  "xaxis_annotation": "visible",
  "yaxis_annotation": "visible",
  "height": 150,
  "annot_options": {
    "annot_horizontal_grid_lines": [
      {
        "y": 100
      },
      {
        "y": 200
      },
      {
        "y": 300
      },
      {
        "y": 400
      }
    ],
    "annot_text_items": []
  },
  "margins": {
    "top": 20,
    "bottom": 40
  },
  "plot_elements": [
    {
      "title": "SILSO observed daily",
      "visible": true,
      "plot_options": {
        "stroke": "#000000",
        "stroke-width": 1,
        "stroke-opacity": "1",
        "fill": "#888888",
        "fill-opacity": 0.09,
        "mirror_zero_x": false,
        "plot_type": "line",
        "curve_type": "linear",
        "baseline_mode": ""
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "sunspot_number_silso_daily",
        "parameter": "sunspot_number",
        "time_parameter": "time",
        "cadence_alternative_datasets_mode": "off",
        "refetch_interval_seconds": 86400,
        "zoom_detail_factor": 1
      },
      "data_transforms": {
        "parameter_multiplier": 1,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "SILSO observed monthly mean",
      "visible": true,
      "plot_options": {
        "stroke": "#000000",
        "stroke-width": 3,
        "stroke-opacity": 0.36,
        "fill": "#888888",
        "fill-opacity": "1",
        "mirror_zero_x": false,
        "plot_type": "line",
        "curve_type": "linear",
        "baseline_mode": ""
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "sunspot_number_silso_monthly",
        "parameter": "sunspot_number_monthly_mean",
        "time_parameter": "time",
        "cadence_alternative_datasets_mode": "",
        "refetch_interval_seconds": 86400,
        "zoom_detail_factor": 1
      },
      "data_transforms": {
        "parameter_multiplier": 1,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "SILSO observed 13-month smoothed",
      "visible": true,
      "plot_options": {
        "stroke": "#000000",
        "stroke-width": 6,
        "stroke-opacity": 1,
        "fill": "#888888",
        "fill-opacity": "1",
        "mirror_zero_x": false,
        "plot_type": "line",
        "curve_type": "linear",
        "baseline_mode": ""
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "sunspot_number_silso_monthly_smoothed",
        "parameter": "sunspot_number_13month_smoothed",
        "time_parameter": "time",
        "cadence_alternative_datasets_mode": "",
        "refetch_interval_seconds": 86400,
        "zoom_detail_factor": 4
      },
      "data_transforms": {
        "parameter_multiplier": 1,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "MSFC forecast 5-95 percentiles",
      "visible": true,
      "plot_options": {
        "plot_type": "area",
        "stroke": "#1e90ff",
        "stroke-width": 0,
        "stroke-opacity": "0.5",
        "fill": "#1e90ff",
        "fill-opacity": 0.13,
        "mirror_zero_x": false,
        "curve_type": "linear",
        "baseline_mode": "parameter",
        "baseline_stroke": "#1e90ff",
        "baseline_stroke-width": "0",
        "baseline_stroke-opacity": "0"
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "sunspot_number_forecast_msfc",
        "parameter": "sunspots_95",
        "time_parameter": "time",
        "issue_time_parameter": "timetag_issue",
        "baseline_parameter": "sunspots_5",
        "cadence_alternative_datasets_mode": "off",
        "refetch_interval_seconds": 86400,
        "zoom_detail_factor": 1
      },
      "data_transforms": {
        "parameter_multiplier": 1,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "MSFC forecast median",
      "visible": true,
      "plot_options": {
        "plot_type": "line",
        "stroke": "#1e90ff",
        "stroke-width": 6,
        "stroke-opacity": "1",
        "fill": "#1e90ff",
        "fill-opacity": "0.1",
        "mirror_zero_x": false,
        "curve_type": "linear"
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "sunspot_number_forecast_msfc",
        "parameter": "sunspots_50",
        "time_parameter": "time",
        "issue_time_parameter": "timetag_issue",
        "cadence_alternative_datasets_mode": "off",
        "refetch_interval_seconds": 86400,
        "zoom_detail_factor": 1
      },
      "data_transforms": {
        "parameter_multiplier": 1,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "NOAA/NASA/ISES panel forecast min/max",
      "visible": true,
      "plot_options": {
        "plot_type": "area",
        "stroke": "#8041c0",
        "stroke-width": 0,
        "stroke-opacity": 0,
        "fill": "#8041c0",
        "fill-opacity": 0.17,
        "mirror_zero_x": false,
        "curve_type": "linear",
        "baseline_mode": "parameter",
        "baseline_stroke": "#8041c0",
        "baseline_stroke-width": "0",
        "baseline_stroke-opacity": "0"
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "sunspot_number_forecast_panel",
        "parameter": "smoothed_ssn_max",
        "time_parameter": "time",
        "baseline_parameter": "smoothed_ssn_min",
        "cadence_alternative_datasets_mode": "off",
        "refetch_interval_seconds": 86400,
        "zoom_detail_factor": 1
      },
      "data_transforms": {
        "parameter_multiplier": 1,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "NOAA/NASA/ISES panel forecast",
      "visible": true,
      "plot_options": {
        "stroke": "#7d29d1",
        "stroke-width": 6,
        "stroke-opacity": "1",
        "fill": "#888888",
        "fill-opacity": "1",
        "mirror_zero_x": false,
        "plot_type": "line",
        "curve_type": "linear",
        "baseline_mode": ""
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "sunspot_number_forecast_panel",
        "parameter": "smoothed_ssn",
        "time_parameter": "time",
        "cadence_alternative_datasets_mode": "off",
        "refetch_interval_seconds": 86400,
        "zoom_detail_factor": 1
      },
      "data_transforms": {
        "parameter_multiplier": 1,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    }
  ],
  "legend_visible": true
}

export const subplot_template_f10_7 =     {
  "title": "10.7 cm solar radio flux",
  "visible": true,
  "ydomain": {
    "default": [
      60,
      250
    ],
    "bounds": [
      0,
      500
    ]
  },
  "y_label": "F10.7 (sfu)",
  "scale": "linear",
  "ytick_format": "d",
  "xaxis_annotation": "visible",
  "yaxis_annotation": "visible",
  "height": 150,
  "annot_options": {
    "annot_horizontal_grid_lines": [
      {
        "y": 100
      },
      {
        "y": 200
      },
      {
        "y": 300
      }
    ],
    "annot_text_items": []
  },
  "margins": {
    "top": 20,
    "bottom": 40
  },
  "plot_elements": [
    {
      "title": "F10.7",
      "visible": true,
      "plot_options": {
        "plot_type": "area",
        "stroke": "#000000",
        "stroke-width": "1",
        "stroke-opacity": "1",
        "fill": "#888888",
        "fill-opacity": 0.25,
        "mirror_zero_x": false,
        "curve_type": "linear",
        "baseline_mode": "constant",
        "baseline": 0,
        "baseline_stroke": "#000000",
        "baseline_stroke-width": "0",
        "baseline_stroke-opacity": "0"
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "f10_7",
        "parameter": "f10_7",
        "time_parameter": "time",
        "baseline_parameter": "f10_7a",
        "cadence_alternative_datasets_mode": "off",
        "refetch_interval_seconds": 86400,
        "zoom_detail_factor": 1
      },
      "data_transforms": {
        "parameter_multiplier": 1,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "F10.7 81-day running mean",
      "visible": true,
      "plot_options": {
        "plot_type": "line",
        "stroke": "#000000",
        "stroke-width": "1",
        "stroke-opacity": "1",
        "fill": "#445588",
        "fill-opacity": 0.25,
        "mirror_zero_x": false,
        "curve_type": "linear",
        "baseline_mode": "constant",
        "baseline": 0,
        "baseline_stroke": "#000000",
        "baseline_stroke-width": "2",
        "baseline_stroke-opacity": "1"
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "f10_7",
        "parameter": "f10_7a",
        "time_parameter": "time",
        "cadence_alternative_datasets_mode": "off",
        "refetch_interval_seconds": 86400,
        "zoom_detail_factor": 1
      },
      "data_transforms": {
        "parameter_multiplier": 1,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "F10.7 observed monthly mean",
      "visible": true,
      "plot_options": {
        "stroke": "#000000",
        "stroke-width": 3,
        "stroke-opacity": 0.36,
        "fill": "#888888",
        "fill-opacity": "1",
        "mirror_zero_x": false,
        "plot_type": "line",
        "curve_type": "linear",
        "baseline_mode": ""
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "f10_7_monthly",
        "parameter": "f10_7_monthly_mean",
        "time_parameter": "time",
        "cadence_alternative_datasets_mode": "",
        "refetch_interval_seconds": 86400,
        "zoom_detail_factor": 1
      },
      "data_transforms": {
        "parameter_multiplier": 1,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "F10.7 13-month smoothed",
      "visible": true,
      "plot_options": {
        "stroke": "#000000",
        "stroke-width": 6,
        "stroke-opacity": 1,
        "fill": "#888888",
        "fill-opacity": "1",
        "mirror_zero_x": false,
        "plot_type": "line",
        "curve_type": "linear",
        "baseline_mode": ""
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "f10_7_monthly",
        "parameter": "f10_7_13month_smoothed",
        "time_parameter": "time",
        "cadence_alternative_datasets_mode": "",
        "refetch_interval_seconds": 86400,
        "zoom_detail_factor": 4
      },
      "data_transforms": {
        "parameter_multiplier": 1,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "MSFC forecast 5-95 percentiles",
      "visible": true,
      "plot_options": {
        "plot_type": "area",
        "stroke": "#1e90ff",
        "stroke-width": 0,
        "stroke-opacity": "0.5",
        "fill": "#1e90ff",
        "fill-opacity": 0.13,
        "mirror_zero_x": false,
        "curve_type": "linear",
        "baseline_mode": "parameter",
        "baseline_stroke": "#1e90ff",
        "baseline_stroke-width": "0",
        "baseline_stroke-opacity": "0"
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "f10_7_forecast_msfc",
        "parameter": "f10_95",
        "time_parameter": "time",
        "issue_time_parameter": "timetag_issue",
        "baseline_parameter": "f10_5",
        "cadence_alternative_datasets_mode": "off",
        "refetch_interval_seconds": 86400,
        "zoom_detail_factor": 1
      },
      "data_transforms": {
        "parameter_multiplier": 1,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "MSFC forecast median",
      "visible": true,
      "plot_options": {
        "plot_type": "line",
        "stroke": "#1e90ff",
        "stroke-width": 6,
        "stroke-opacity": "1",
        "fill": "#1e90ff",
        "fill-opacity": "0.1",
        "mirror_zero_x": false,
        "curve_type": "linear"
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "f10_7_forecast_msfc",
        "parameter": "f10_50",
        "time_parameter": "time",
        "issue_time_parameter": "timetag_issue",
        "cadence_alternative_datasets_mode": "off",
        "refetch_interval_seconds": 86400,
        "zoom_detail_factor": 1
      },
      "data_transforms": {
        "parameter_multiplier": 1,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "NOAA/NASA/ISES panel forecast min/max",
      "visible": true,
      "plot_options": {
        "plot_type": "area",
        "stroke": "#8041c0",
        "stroke-width": 0,
        "stroke-opacity": 0,
        "fill": "#8041c0",
        "fill-opacity": 0.17,
        "mirror_zero_x": false,
        "curve_type": "linear",
        "baseline_mode": "parameter",
        "baseline_stroke": "#8041c0",
        "baseline_stroke-width": "0",
        "baseline_stroke-opacity": "0"
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "f10_7_forecast_panel",
        "parameter": "smoothed_f10_7_max",
        "time_parameter": "time",
        "baseline_parameter": "smoothed_f10_7_min",
        "cadence_alternative_datasets_mode": "off",
        "refetch_interval_seconds": 86400,
        "zoom_detail_factor": 1
      },
      "data_transforms": {
        "parameter_multiplier": 1,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "NOAA/NASA/ISES panel forecast",
      "visible": true,
      "plot_options": {
        "stroke": "#7d29d1",
        "stroke-width": 6,
        "stroke-opacity": "1",
        "fill": "#888888",
        "fill-opacity": "1",
        "mirror_zero_x": false,
        "plot_type": "line",
        "curve_type": "linear",
        "baseline_mode": ""
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "f10_7_forecast_panel",
        "parameter": "smoothed_f10_7",
        "time_parameter": "time",
        "cadence_alternative_datasets_mode": "off",
        "refetch_interval_seconds": 86400,
        "zoom_detail_factor": 1
      },
      "data_transforms": {
        "parameter_multiplier": 1,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    }
  ]
}

export const subplot_template_swarm_ne =     {
  "title": "Swarm OPER electron density",
  "visible": true,
  "legend_visible": true,
  "ydomain": {
    "default": [
      0,
      3
    ],
    "bounds": [
      0,
      6
    ]
  },
  "y_label": "Ne (10⁶/cm³)",
  "scale": "linear",
  "ytick_format": "auto",
  "xaxis_annotation": "hidden",
  "yaxis_annotation": "visible",
  "height": 130,
  "annot_options": {
    "annot_horizontal_grid_lines": [],
    "annot_text_items": []
  },
  "margins": {
    "top": 20,
    "bottom": 10
  },
  "plot_elements": [
    {
      "title": "Swarm A Ne long-term min/max",
      "visible": true,
      "plot_options": {
        "plot_type": "area",
        "stroke": "#006CD9",
        "stroke-width": 0,
        "stroke-opacity": "1",
        "fill": "#006CD9",
        "fill-opacity": 0.1,
        "mirror_zero_x": false,
        "curve_type": "step",
        "baseline_mode": "parameter"
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "sw_oper_efia_lp_aggregate",
        "parameter": "Ne_max",
        "time_parameter": "time",
        "baseline_parameter": "Ne_min",
        "cadence_alternative_datasets_mode": "x_relations",
        "zoom_detail_factor": 1.0
      },
      "data_transforms": {
        "parameter_multiplier": 1e-06,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "Swarm B Ne long-term min/max",
      "visible": true,
      "plot_options": {
        "plot_type": "area",
        "stroke": "#A070DC",
        "stroke-width": 0,
        "stroke-opacity": "1",
        "fill": "#A070DC",
        "fill-opacity": 0.1,
        "mirror_zero_x": false,
        "curve_type": "step",
        "baseline_mode": "parameter"
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "sw_oper_efib_lp_aggregate",
        "parameter": "Ne_max",
        "time_parameter": "time",
        "baseline_parameter": "Ne_min",
        "cadence_alternative_datasets_mode": "x_relations",
        "zoom_detail_factor": 1.0
      },
      "data_transforms": {
        "parameter_multiplier": 1e-06,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "Swarm A Ne long-term mean",
      "visible": true,
      "plot_options": {
        "stroke": "#006CD9",
        "stroke-width": 1,
        "stroke-opacity": 1,
        "fill": "#006CD9",
        "fill-opacity": "1",
        "mirror_zero_x": false,
        "plot_type": "line",
        "curve_type": "step",
        "baseline_mode": ""
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "sw_oper_efia_lp_aggregate",
        "parameter": "Ne_mean",
        "time_parameter": "time",
        "cadence_alternative_datasets_mode": "x_relations",
        "zoom_detail_factor": 1.0
      },
      "data_transforms": {
        "parameter_multiplier": 1e-06,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "Swarm B Ne long-term mean",
      "visible": true,
      "plot_options": {
        "stroke": "#A070DC",
        "stroke-width": 1,
        "stroke-opacity": 1,
        "fill": "#A070DC",
        "fill-opacity": "1",
        "mirror_zero_x": false,
        "plot_type": "line",
        "curve_type": "step",
        "baseline_mode": ""
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "sw_oper_efib_lp_aggregate",
        "parameter": "Ne_mean",
        "time_parameter": "time",
        "cadence_alternative_datasets_mode": "x_relations",
        "zoom_detail_factor": 1.0
      },
      "data_transforms": {
        "parameter_multiplier": 1e-06,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "Swarm A Ne 2Hz OPER data",
      "visible": true,
      "plot_options": {
        "plot_type": "line",
        "stroke": "#006CD9",
        "stroke-width": 2.5,
        "stroke-opacity": 1,
        "fill": "#006CD9",
        "fill-opacity": 0.16,
        "mirror_zero_x": false,
        "curve_type": "linear",
        "baseline_mode": ""
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "sw_oper_efia_lp",
        "parameter": "Ne",
        "time_parameter": "time",
        "cadence_alternative_datasets_mode": "x_relations",
        "zoom_detail_factor": 1.0
      },
      "data_transforms": {
        "parameter_multiplier": 1e-06,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "Swarm B Ne 2Hz OPER data",
      "visible": true,
      "plot_options": {
        "plot_type": "line",
        "stroke": "#A070DC",
        "stroke-width": 2.5,
        "stroke-opacity": 1,
        "fill": "#A070DC",
        "fill-opacity": 0.16,
        "mirror_zero_x": false,
        "curve_type": "linear",
        "baseline_mode": ""
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "sw_oper_efib_lp",
        "parameter": "Ne",
        "time_parameter": "time",
        "cadence_alternative_datasets_mode": "x_relations",
        "zoom_detail_factor": 1.0
      },
      "data_transforms": {
        "parameter_multiplier": 1e-06,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    },
    {
      "title": "Swarm C Ne 2Hz OPER data",
      "visible": true,
      "plot_options": {
        "plot_type": "line",
        "stroke": "#70B8FF",
        "stroke-width": 2.5,
        "stroke-opacity": 1,
        "fill": "#70B8FF",
        "fill-opacity": 0.16,
        "mirror_zero_x": false,
        "curve_type": "linear",
        "baseline_mode": ""
      },
      "data": {
        "server": "https://data.spaceweather.knmi.nl/hapi",
        "dataset": "sw_oper_efic_lp",
        "parameter": "Ne",
        "time_parameter": "time",
        "cadence_alternative_datasets_mode": "x_relations",
        "zoom_detail_factor": 1.0
      },
      "data_transforms": {
        "parameter_multiplier": 1e-06,
        "parameter_offset": 0,
        "parameter_offset_remove_mean": false
      }
    }
  ]
}

import { interpolateRdBu,
 interpolateRdYlBu,
 interpolateSpectral,
 interpolateBlues,
 interpolateGreens,
 interpolateGreys,
 interpolateOranges,
 interpolatePurples,
 interpolateReds,
 interpolateTurbo,
 interpolateViridis,
 interpolateInferno,
 interpolateMagma,
 interpolatePlasma,
 interpolateCividis,
 interpolateWarm,
 interpolateCool,
 interpolateCubehelixDefault,
 interpolateRainbow,
 interpolateSinebow } from 'd3';

export const color_schemes = {
    'RdBu': interpolateRdBu,
    'RdYlBu': interpolateRdYlBu,
    'Spectral': interpolateSpectral,
    'Blues': interpolateBlues,
    'Greens': interpolateGreens,
    'Greys': interpolateGreys,
    'Oranges': interpolateOranges,
    'Purples': interpolatePurples,
    'Reds': interpolateReds,
    'Turbo': interpolateTurbo,
    'Viridis': interpolateViridis,
    'Inferno': interpolateInferno,
    'Magma': interpolateMagma,
    'Plasma': interpolatePlasma,
    'Cividis': interpolateCividis,
    'Warm': interpolateWarm,
    'Cool': interpolateCool,
    'Cubehelix': interpolateCubehelixDefault,
    'Rainbow': interpolateRainbow,
    'Sinebow': interpolateSinebow,
}

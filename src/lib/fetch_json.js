export const fetch_json = async function({queryKey}) {
    console.log(queryKey)
    const [_key, url] = queryKey;
    // console.log("fetch_json" , queryKey);
    const response = await fetch(url);
    if (!response.ok) {
        throw new Error('Network response was not ok on fetch of url: ' + url);
    }
    try {
        const response_object = await response.json();
        return response_object;
    } catch(error) {
        throw new Error("Error parsing json of url: " + url + ". The error was: " + error);
    }
}
